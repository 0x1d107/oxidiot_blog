# Installation
This site requires python 3 to build.
```bash
git clone https://gitlab.com/0x1d107/oxidiot_blog.git
cd oxidiot_blog
```
It is recommended to setup a virtual environment in this repository 
```bash
python -m venv .
source bin/activate
```
The install the required python modules
```bash
python -m pip install -r requirements.txt
```
`pelican-katex` requires nodejs to work. Install it with your local package manager.
```bash
pacman -S nodejs

```
**[Important for publishing]** Create new branch named `pages` for the output html.
This branch will be generated automatically and it shouldn't be modified manually!
```bash
git branch pages
```
Add github pages repo as a remote named `github`
```bash
git remote add github https://github.com/0x1d107/0x1d107.github.io.git
```
# Building
Activate python virtual environment.
```bash
source bin/activate
```
Then generate HTML pages
```bash
cd site
make html 
```
generated html will be in output directory. After you've ended editing the site you can deactivate the virtual environment.

```bash
deactivate
```
# Publishing
Activate python virtual environment.
```bash
source bin/activate
```
The generate the html pages and put them in branch called `pages`.
Push the `pages` branch to the `master` branch of the remote `github`.
```bash
cd site
make github 
```



