#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import pelican_katex

AUTHOR = 'oxidiot'
SITENAME = 'Blog id:0x1d107'
SITEURL = ''

PATH = 'content'

THEME = 'theme'

TIMEZONE = 'Asia/Yekaterinburg'

DEFAULT_LANG = 'en'

PLUGINS= [pelican_katex]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# Blogroll
LINKS = ()
# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
