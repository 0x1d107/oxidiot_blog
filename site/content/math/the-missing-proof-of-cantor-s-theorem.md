Title: The missing proof of Cantor's theorem
Date: Sat  3 Oct 22:31:43 +05 2020
Category: math
Author: Oxidiot

While preparing for exam, I've found out that the proof of Cantor's theorem is missing in our lecture notes. This is a question on our future exam, so I decided to write it here.

*Theorem*: $|A| < |B(A)|$, where $B(A)$ is a set of all subsets of A.

*Proof*:

$$
    \forall x \in A \exists \{x\} \in B(A) \implies |A| \leq |B(A)| 
$$

Then we need to prove that there isn't any bijection from $A$ to $B(A)$

Proof by contradiction: Assume $\exists f:A \leftrightarrow B(A)$ 


$$
    T := \{x \in A : x \notin f(x) \}
$$


$$
    T \in B(A) \implies \exists t: f(t) = T \implies \neg ( t \in T) \land \neg (t \notin T) \implies \bot \blacksquare
$$
    
