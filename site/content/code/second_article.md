Title: Second article about code 
Date: Mon May 18 14:32:44 +05 2020
Category: code

Testing pygments code highlighting
```
:::python
import numpy as np
a = np.array([[3,2],[5,6]])
```
This code constructs Numpy array with values (rendered statically with katex)
$\begin{bmatrix}3&2\\5&6\end{bmatrix}$
